# Create a JSON-encoded list of regular-users
# Example:
#
# [{"user":"root", "id":"0", "group":"0"},{"user":"daemon", "id":"1", "group":"1"},
#  {"user":"xinlei", "id":"57009", "group":"37"},{"user":"mgoll", "id":"48805", "group":"0"},
#  {"user":"stunnel4", "id":"109", "group":"114"}
# ]

Facter.add("regular_users") do
  setcode do
    passwd_file_name = '/etc/passwd'
    if (File.exist?(passwd_file_name))
      passwd_file = IO.read(passwd_file_name)
      lines = passwd_file.split(/\n/)
      results = []
      lines.each do |l|
        #          games:x:5:60:games:/usr/games:/bin/sh
        match = /^([^:]+):([^:]+):(\d+):(\d+):/.match(l)
        if ((!match.nil?) && match.length >= 3)
          name  = match[1]
          id    = match[3]
          group = match[4]

          results.push(%Q[{"user":"#{name}","id":"#{id}","group":"#{group}"}])
        else
          raise "could not parse '#{l}'"
        end
      end
      "[" + results.join(",") + "]"
    else
      raise "could not find file '#{passwd_file_name}'"
    end
  end
end


# Create a JSON-encoded list of users in /root/.k5login
# Example:
#
# ["adamhl/root@stanford.edu","bxk/root@stanford.edu","digant/root@stanford.edu",
#  "frobozz/root@stanford.edu","hallk/root@stanford.edu","jcowart/root@stanford.edu",
#  "jonrober/root@stanford.edu","mgoll/root@stanford.edu","pradtke/root@stanford.edu",
#  "rra/root@stanford.edu","saracook/root@stanford.edu","sfeng/root@stanford.edu",
#  "whm/root@stanford.edu","yuelu/root@stanford.edu"
# ]

Facter.add("root_users") do
  setcode do
    results = []
    k5login_filename = '/root/.k5login'
    if (File.exist?(k5login_filename))
      k5login_file = IO.read(k5login_filename)
      lines = k5login_file.split(/\n/)
      lines.each do |l|
        l.strip!()
        if (!l.empty?())
          results.push(%Q["#{l}"])
        end
      end
    end
    "[" + results.join(",") + "]"
  end
end

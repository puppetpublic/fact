# Fact: fqdn_lc
#
# Purpose: Returns the fully qualified, lower-cased, domain name of the host.
#
# Resolution: Simply joins the hostname fact with the domain name fact.
#
# Caveats: No attempt is made to check that the two facts are accurate or that
# the two facts go together. At no point is there any DNS resolution made
# either.

Facter.add(:fqdn_lc) do
  setcode do
    host = Facter.value(:hostname).downcase()
    domain = Facter.value(:domain).downcase()
    if host and domain
      [host, domain].join(".")
    else
      nil
    end
  end
end

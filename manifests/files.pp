# Create a directory to hold files created by the fact define.  This is a
# separate class so that it can be included from the fact define and doesn't
# create conflicts among multiple facts.  This directory is used by the sufact
# facter plugin.

class fact::files {
  file { '/var/lib/puppet/sufact':
    ensure  => directory,
    recurse => true,
    force   => true,
    purge   => true;
  }
}

# Provides arbitrary named facts, which are loaded into facter by the sufact
# plugin module.
#
# Examples:
#
#     fact { 'su_sysadmin0':  value => 'SunetID'  }
#     fact { 'su_sysadmin1':  value => 'SunetID2' }
#     fact { 'su_support':    value => 'IDG'      }
#     fact { 'su_restricted': value => 'true'     }

define fact(
  $ensure = present,
  $value  = '',
) {
  include fact::files
  include fact::common

  if ($ensure == 'present') and ($value == '') {
    fail("No value provided for ${name} fact")
  }

  file { "/var/lib/puppet/sufact/${name}":
    ensure  => $ensure,
    content => "${value}\n",
  }
}

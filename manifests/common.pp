# fact::common contains common facts.  It is a class, so it can be included 
# multiple times but will only be compiled once.
# Users of this shared module should `include fact::common` in their defaults 
# as a safety measure.  That being said, the fact type does include this class, 
# so clients which define at least one custom fact should be fine.
class fact::common {
  # Make sure we have the basic sufact directory
  include fact::files
 
  # Now, let's start populating some common facts!
 
  # Generate a fact for the server's Livermore presence.
  if (   ip_in_cidr($::ipaddress, '204.63.224.0/21')
      or ip_in_cidr($::ipaddress, '172.20.224.0/21')
  ) {
    $livermore = "1"
  }
  else {
    $livermore = "0"
  }
  file { '/var/lib/puppet/sufact/su_livermore':
    ensure  => present,
    content => "$livermore\n"
  }

}
